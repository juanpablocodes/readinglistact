/*
Folder and Files Preparation: 

1. Create a "readingListAct" Folder in your Batch Folder. 
2. Create an index.html and index.js file. For the index.html title just write "Reading List Activity 1" 
3. Show your solutions for each problem in the index.js file. 
4. Once done, Create a remote repository named "readingListAct".
5. Save your repo link in Boodle: WDC028V1.5b-18-C1 | JavaScript - Function Parameters, Return Statement and Array Manipulations


*/


// Activity Template:
// (Copy this part on your template)


/*
	1.) Create a function that returns a passed string with letters in alphabetical order.
		Example string: 'mastermind'
		Expected output: 'adeimmnrst'
	
*/

// Code here:

function showAlpOrder(word) {
	return word.split('').sort().join('');
}
console.log(showAlpOrder("mastermind"));

/*
	2.) Write a simple JavaScript program to join all elements of the following array into a string.

	Sample array : myColor = ["Red", "Green", "White", "Black"];
		Expected output: 
			Red,Green,White,Black
			Red,Green,White,Black
			Red+Green+White+Black

*/

// Code here:
let myColor = ["Red", "Green", "White", "Black"];

console.log(myColor.join());
console.log(myColor.join(''));
console.log(myColor.join('+'))


/*
	3.) Write a function named birthdayGift that pass a gift as an argument.
		- if the gift is a stuffed toy, return a message that says: "Thank you for the stuffed toy, Michael!"
		- if the gift is a doll, return a message that says: "Thank you for the doll, Sarah!"
		- if the gift is a cake, return a message that says: "Thank you for the cake, Donna!"
		- if other gifts return a message that says: "Thank you for the (gift), Dad!"
		- create a global variable named myGift and invoke the function in the variable.
		- log the global var in the console

*/

// Code here:

function birthdayGift(gift) {
	if (gift == 'stuffed toy') {
			return "Thank you for the stuffed toy, Michael!"
	} if (gift == 'doll') {
			return "Thank you for the doll, Sarah!"
	} if (gift == 'cake') {
		return "Thank you for the cake, Donna!"
	} else {
		return "Thank you for the (gift), Dad!"
	}
}

let myGift = birthdayGift('cake');
console.log(myGift);


/*
	4.) Write a function that accepts a string as a parameter and counts the number of vowels within the string.

		Example string: 'The quick brown fox'
		Expected Output: 5

*/

// Code here:
function getVowels(fox) {
	let count = 0
	const vowels = ['a', 'e', 'i', 'o', 'u'];

	for (let letter of fox.toLowerCase()) {
		for(let v of vowels){
			if (letter===v){
				count++;
			}
		}
	}
	return count;
}

let string = 'The quick brown fox';
console.log(getVowels(string));
